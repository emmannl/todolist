# TodoList

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

Simple todo list manager based on the Laravel PHP framework
## Installation
The package requires the Laravel framework.

Install via Composer:

In your Laravel project folder run:
``` bash
$ composer require dotbrainy/todolist
```
For Laravel v5.4 or below, you need to manually add the package's service provider to the `provider` key in your `config/app.php` file, like so:
```php
'providers' => [
//...
    dotBrainy\TodoList\TodoListServiceProvider::class
}
```

Next publish the package's public assets and config file
```bash
$  php artisan vendor:publish --tag=public

$  php artisan vendor:publish --tag=config
```

Finally run migrations to create the needed database structures
```bash
$ php artisan migrate
```


## Usage
Visit `http(s)::/{site_url}/to-do`

Start by adding a task category, then some todo tasks to that category

##Note
The package loads the necessary Javascript files from respective CDN. If you would like to use the local version 
of the files, edit `config/todolist.php`  and change the value of `use_cdn`  to `false`
## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## License

Apache Licence 2.0. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/dotbrainy/todolist.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/dotbrainy/todolist.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/dotbrainy/todolist/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/dotbrainy/todolist
[link-downloads]: https://packagist.org/packages/dotbrainy/todolist
[link-travis]: https://travis-ci.org/dotbrainy/todolist
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/dotbrainy
[link-contributors]: ../../contributors
