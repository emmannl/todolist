<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('web')->namespace('dotBrainy\TodoList\Http\Controllers')->prefix('to-do')->group( function() {

    Route::get('/', 'TasksController@index')->name('tasks.index');

    Route::post('create', 'TasksController@createTask')->name('tasks.create');

    Route::post('change_status', 'TasksController@changeTaskStatus')->name('tasks.status.change');

    Route::get('delete/{id}', 'TasksController@deleteTask')->name('tasks.delete');


    Route::prefix('categories')->group( function() {

        Route::post('create', 'TasksController@createCategory')->name('tasks.cat.create');

        Route::post('show', 'TasksController@showCategory')->name('categories.single');
        Route::patch('show', 'TasksController@updateCategory');

        Route::delete('delete', 'TasksController@deleteCategory')->name('tasks.cat.delete');

    });

});

/*Route::resource('tasks/cat', 'TaskCategoriesController')->names([
	'index' => 'tasks.cat.index',
	'create' => 'tasks.cat.create',
	'store' => 'tasks.cat.store',
	'show' => 'tasks.cat.show',
	'edit' => 'tasks.cat.edit',
	'update' => 'tasks.cat.update'
]);
*/
