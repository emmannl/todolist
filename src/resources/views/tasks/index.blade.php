@extends('todoList::tasks.layout')
@section('content')
@include('todoList::tasks.partials.nav')
<div class="my-4">
	<!-- Category list -->
	<div class="col-md-8 offset-md-2">
		<div class="row">
			<div class="col-sm-4">
				<form id="addCat" action="{{ route('tasks.cat.create') }}" class="input-group collapse" method="post">
					@csrf
					<input type="text" class="form-control" name="name" placeholder="Add Category">
					<button type="submit" class="input-group-append btn btn-success">Add</button>
				</form>
			</div>
		</div>
		<div class="row">
			<p>
				<a class="btn btn-link text-decoration-none" data-toggle="collapse" href="#addCat" role="button" aria-expanded="false" aria-controls="collapseExample">
					<i class="fas fa-plus-circle"></i> Add Category
				</a>
			</p>
		</div>
		<div class="d-flex">
			@if( !$categories)
			<div>No vagaries found</div>
			@else
			@foreach($categories as $cat)
				<div class="rounded mx-2 px-2 bg-info">
					<a href="{{ route('tasks.index') }}?category={{ $cat->slug }}" class="text-white">
					{{ $cat->name }}
					</a> &nbsp;
					<form action="{{ route('tasks.delete', $cat->id) }}" class="form-inline float-right" method="post">
						<input type="hidden" name="id" value="{{ $cat->id }}">
						<a class="text-danger" href="#deleteModal" data-toggle="modal" onclick="makeDeleteForm(`{{ $cat->id }}`)"><i class="far fa-trash-alt"></i></a>
					</form> &nbsp;
					<a href="#EditModal" class="text-warning float-right EditModalLink" id="c{{ $cat->id }}" data-toggle="modal" ><i class="far fa-edit"></i></a>
				</div>
			@endforeach
			@endif
		</div>
	</div>
	<!-- TodoItems -->
	<div class="col-md-8 offset-md-2 mt-4">
		<h4>{{ !is_null($current_category) ? 'Category: ' . ucwords($current_category->name) : '' }}</h4>
		@if(is_null($pending_tasks) || is_null($completed_tasks))
			<p>Select a category to view tasks</p>
		@else
			<form id="addTask" action="{{ route('tasks.create') }}" method="post" class="form-row mb-5 collapse"
				  onsubmit="return submit_a_form('addTask')">
				@csrf
				<input type="text" class="form-control col-sm-8" name="title" placeholder="Add new Task">
				<input type="hidden" name="category_id" value="{{ $current_category->id }}">
				<button class="btn btn-success col-sm-2" type="submit">Add</button>
				<button class="btn btn-danger col-sm-2" type="button" data-toggle="collapse" data-target="#addTask"">
				Cancel
				</button>
			</form>
			<p>
				<a class="btn btn-link text-decoration-none" data-toggle="collapse" href="#addTask" role="button" aria-expanded="false" aria-controls="collapseExample">
					<i class="fas fa-plus-circle"></i> Add Task
				</a>
			</p>
			@if(count($current_category->tasks) < 1 )
				<p>There are no tasks in this category, you may now add one.</p>
			@else
				<table class="table table-striped">
				<tr>
					<th colspan="3" class="bg-warning">Pending Item</th>
				</tr>
				@foreach($pending_tasks as $task)
					<tr>
						<td>
							<input type="checkbox" onchange="switchStatus({{ $task->id }})" class="form-check"{{ $task->completed ? ' checked' : '' }}>
						</td>
						<td>
							{{ $task->title }}
							<br>
							<small class="text-secondary">Added {{ $task->created_at->toFormattedDateString() }}</small>
						</td>
						<td>
							<a href="{{ route('tasks.delete', $task->id) }}" class="text-danger"> <i class="far fa-trash-alt"></i></a>
						</td>
					</tr>
				@endforeach
				<tr class="bg-secondary">
					<th colspan="3">Done Items</th>
				</tr>
				@foreach($completed_tasks as $task)
					<tr>
						<td>
							<input type="checkbox" onchange="switchStatus({{ $task->id }})" class="form-check"{{ $task->completed ? ' checked' : '' }}>
						</td>
						<td>
							<s> {{ $task->title }} </s>
							<br>
							<small class="text-secondary">Added {{ $task->created_at->toFormattedDateString() }}</small>
						</td>
						<td>
							<a href="{{ route('tasks.delete', $task->id) }}" class="text-danger"> <i class="far fa-trash-alt"></i></a>
						</td>
					</tr>
				@endforeach
				</table>
			@endif
		@endif
	</div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editTitle">Edit </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('categories.single') }}" method="post" id="editForm">
                    @method('patch')
                    @csrf
                    <label for="editName">Category Name</label>
                    <input type="text" name="name" value="" required class="form-control" placeholder="Category Name" id="editName">
                    <input type="hidden" name="category_id" id="editId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="submitEdit">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editTitle">Delete Category </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('tasks.cat.delete') }}" method="post" id="deleteForm">
                    @method('delete')
                    @csrf
                    <p class="lead">
                        All tasks in the category will also be deleted!
                    </p>
                    <input type="hidden" name="category_id" id="deleteId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="submitDelete">Delete</button>
            </div>
        </div>
    </div>
</div>



<!-- Toast Flash -->
@include('todoList::tasks.partials.toast')
<script>

	//Switch Task Status
    function switchStatus(task_id)
    {
        let form = new FormData;
        form.append('_token', `{{ csrf_token() }}`);
        form.append(`id`, task_id);
        axios.post(`{{ route('tasks.status.change') }}`, form)
            .then( function (res) {
                console.log(res.date)
                window.location.reload();
            })
            .catch( function (err) {
                console.log(err.response.data)
            });
    }

    //Fill the edit modal with category contents
    $('.EditModalLink').click(function () {
        let cat_id = this.id.substr(1);
        let form = new FormData;
        form.append('_token', '{{ csrf_token() }}');
        form.append('id', cat_id);
        axios.post(`{{ route('categories.single') }}`, form)
            .then( function (res) {
                $('#editTitle').text(`Edit ${res.data.name}`);
                $('#editName').val(res.data.name);
                $('#editId').val(cat_id);
            })
            .catch( function (err) {
                console.log(err.response.data);
            });
    });

    //reset Edit modal, if editing is cancelled
    $('#EditModal').on('hide.bs.modal', function (e) {
        $('#editTitle').text(`Edit`);
        $('#editName').val('');
        $('#editId').val('');
    });

    //Submit the edit Category form
    $('#submitEdit').click( function () {
        $('#editForm').submit();
    });

	function makeDeleteForm(cat_id) {
        //Submit the delete form, if the delete button is clicked
        $('#submitDelete').click( function () {
            $('#deleteId').val(cat_id);
            $('#deleteForm').submit();
        });
	}

</script>
@endsection
