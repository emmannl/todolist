<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="{{ asset('vendor/dotBrainy/todo-list/css/bootstrap-darkly.min.css') }}">
    @if(config('todolist.use_cdn') === true)
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    @else
        <script src="{{ asset('vendor/dotBrainy/todo-list/js/axios.min.js')  }}"></script>
        <script src="{{ asset('vendor/dotBrainy/todo-list/js/jquery-3.3.1.min.jss') }}"></script>
        <script src="{{ asset('vendor/dotBrainy/todo-list/js/bootstrap.min.js') }}"></script>
    @endif
    <title>Todo List Manager</title>
</head>
<body>
    <main id="app" class="bg-light min-vh-100">
    @yield('content')
    </main>
</body>
</html>
