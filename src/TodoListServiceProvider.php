<?php

namespace dotBrainy\TodoList;

use Illuminate\Support\ServiceProvider;

class TodoListServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
         $this->loadViewsFrom(__DIR__.'/resources/views', 'todoList');
         $this->loadMigrationsFrom(__DIR__.'/database/migrations');
         $this->loadRoutesFrom(__DIR__.'/routes.php');

		$this->mergeConfigFrom(
			__DIR__.'/config/todolist.php', 'todoList'
		);

		$this->publishes([
			__DIR__ . '/config/todolist.php' => config_path('todolist.php'),
		], 'config');

		$this->publishes([
         	__DIR__ . '/assets' => public_path('vendor/dotBrainy/todo-list')
		 ], 'public');

	}

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        // Register the service the package provides.
        $this->app->singleton('todoList', function ($app) {
            return new TodoList;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['todoList'];
    }
}
