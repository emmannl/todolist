<?php

namespace dotBrainy\TodoList\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected  $table = 'dbr_tasks';
}
