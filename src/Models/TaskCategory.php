<?php

namespace dotBrainy\TodoList\Models;

use Illuminate\Database\Eloquent\Model;

class TaskCategory extends Model
{
    protected  $table = 'dbr_tasks_categories';

    public  function  tasks()
    {
        return $this->hasMany('dotBrainy\TodoList\Models\Task', 'category_id');
    }

}
