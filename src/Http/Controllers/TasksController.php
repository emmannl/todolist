<?php

namespace dotBrainy\TodoList\Http\Controllers;

use dotBrainy\TodoList\Models\Task;
use dotBrainy\TodoList\Models\TaskCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class TasksController extends Controller
{
    public function index(Request $request)
    {
        $categories = TaskCategory::all();

        $this_category = $request->get('category');

        $this_category = !empty($this_category) ? TaskCategory::where('slug', $request->get('category'))->with('tasks')->first() : null;

        if (!empty($this_category)) {
			$completed_tasks = $this_category->tasks->where('completed', 1) ?? null;
			$pending_tasks = $this_category->tasks->where('completed', 0) ?? null;
		}


        return view('todoList::tasks.index', [
            'categories' => $categories,
            'completed_tasks' => $completed_tasks ?? null,
            'pending_tasks' => $pending_tasks ?? null,
            'current_category' => $this_category ?? null
        ]);
    }

    public function createCategory(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255|unique:dbr_tasks_categories,name',
        ]);
        $category = new TaskCategory;
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->save();

        $url = route('tasks.index') . '?category=' . $category->slug;

		toast()->message('Category add success', 'success', 'Notice');
        return redirect($url);
    }

    public function createTask(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'title' => 'required|string|max:255'
        ]);

        if ($validate->fails()) {
			toast()->message('the given input did not pass validation', 'danger', 'Invalid Input');
			return back();
		}

        $task = new Task;
        $task->title = $request->title;
        $task->category_id = $request->category_id;
        $task->save();

		toast()->message('Task add success', 'success', 'Notice');
        return back();
    }

    public function changeTaskStatus(Request $request)
    {
        $task = Task::findOrFail($request->id);
        $task->completed = $task->completed == true ? false : true;
        $task->save();

		toast()->message('Task status change success', 'success', 'Notice');
		return back();
    }

    public  function showCategory(Request $request)
    {
        $id = $request->id;
        $category = TaskCategory::findOrFail($id);

        return $category;
    }
    public function updateCategory(Request $request)
    {
        $category = TaskCategory::findOrFail($request->category_id);
        $category->name = $request->name;
        $category->slug = Str::slug($request->name);
        $category->save();

        $url = route('tasks.index') . "?category=$category->slug";

		toast()->message('Category edit success', 'success', 'Notice');
		return redirect($url);
    }

    public function deleteTask($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return back();
    }

    public function  deleteCategory(Request $request)
    {
        $category = TaskCategory::findOrFail($request->category_id);
        $category->tasks()->delete();
        $category->delete();

		toast()->message('Category deleted successfully', 'info', 'Notice');
        return redirect(route('tasks.index'));

    }
}
