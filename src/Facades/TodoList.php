<?php

namespace dotBrainy\TodoList\Facades;

use Illuminate\Support\Facades\Facade;

class TodoList extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'todolist';
    }
}
